# SPDX-FileCopyrightText: 2023 Scarlett Moore <sgmoore@kde.org>
#
# SPDX-License-Identifier: CC0-1.0
---
name: skrooge
adopt-info: skrooge
confinement: strict
grade: stable
base: core22
apps:
    skrooge:
        # extensions:
        # - kde-neon
        command: usr/bin/skrooge
        desktop: usr/share/applications/org.kde.skrooge.desktop
        common-id: org.kde.skrooge
        plugs:
        - network-manager
        - audio-playback
        - home
        - desktop
        - desktop-legacy
        - opengl
        - wayland
        - x11
        - unity7
        - network
        - network-bind
        - browser-support
        - cups
        command-chain:
        - snap/command-chain/desktop-launch
assumes:
- snapd2.58.3
compression: lzo
plugs:
    desktop:
        mount-host-font-cache: false
    icon-themes:
        interface: content
        target: $SNAP/data-dir/icons
        default-provider: gtk-common-themes
    sound-themes:
        interface: content
        target: $SNAP/data-dir/sounds
        default-provider: gtk-common-themes
    kf5-5-110-qt-5-15-11-core22:
        content: kf5-5-110-qt-5-15-11-core22-all
        interface: content
        default-provider: kf5-5-110-qt-5-15-11-core22
        target: $SNAP/kf5
    foo-install-cups:
        interface: content
        content: foo
        default-provider: cups
        target: $SNAP_DATA/foo
environment:
    SNAP_DESKTOP_RUNTIME: $SNAP/kf5
    GST_PLUGIN_PATH: "$SNAP/kf5/usr/lib/$CRAFT_ARCH_TRIPLET/gstreamer-1.0"
    GST_PLUGIN_SYSTEM_PATH: "$SNAP/kf5/usr/lib/$CRAFT_ARCH_TRIPLET/gstreamer-1.0"
    GST_PLUGIN_SCANNER: "$SNAP/kf5/usr/lib/$CRAFT_ARCH_TRIPLET/gstreamer1.0/gstreamer-1.0/gst-plugin-scanner"
    QTWEBENGINEPROCESS_PATH: "$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET/qt5/libexec/QtWebEngineProcess"
    QT_PLUGIN_PATH: "$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET/qt5/plugins:$SNAP/kf5/usr/lib/$CRAFT_ARCH_TRIPLET/qt5/plugins/kf5:$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET/plugins"
hooks:
    configure:
        plugs:
        - desktop
        command-chain:
        - snap/command-chain/hooks-configure-desktop
layout:
    /usr/share/X11:
        symlink: $SNAP/kf5/usr/share/X11
    "/usr/lib/$CRAFT_ARCH_TRIPLET/alsa-lib":
        bind: "$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET/alsa-lib"
    "/usr/share/alsa":
        bind: "$SNAP/usr/share/alsa"
    /usr/share/qt5:
        bind: $SNAP/usr/share/qt5
slots:
    session-dbus-interface:
        interface: dbus
        name: org.kde.skrooge
        bus: session
package-repositories:
-   type: apt
    components:
    - main
    suites:
    - jammy
    key-id: 444DABCF3667D0283F894EDDE6D4736255751E5D
    url: http://origin.archive.neon.kde.org/user
    key-server: keyserver.ubuntu.com
parts:
    kde-neon:
        source: /snap/snapcraft/current/share/snapcraft/extensions/desktop/kde-neon
        source-type: local
        plugin: make
        make-parameters:
        - PLATFORM_PLUG=kf5-5-110-qt-5-15-11-core22
        build-snaps:
        - kf5-5-110-qt-5-15-11-core22-sdk
        build-environment:
        - &id001
            PATH: /snap/kf5-5-110-qt-5-15-11-core22-sdk/current/usr/bin${PATH:+:$PATH}
        - &id002
            XDG_DATA_DIRS: $CRAFT_STAGE/usr/share:/snap/kf5-5-110-qt-5-15-11-core22-sdk/current/usr/share:/usr/share${XDG_DATA_DIRS:+:$XDG_DATA_DIRS}
        - &id003
            XDG_CONFIG_HOME: $CRAFT_STAGE/etc/xdg:/snap/kf5-5-110-qt-5-15-11-core22-sdk/current/etc/xdg:/etc/xdg${XDG_CONFIG_HOME:+:$XDG_CONFIG_HOME}
        - &id004
            CRAFT_CMAKE_ARGS: -DCMAKE_FIND_ROOT_PATH=/snap/kf5-5-110-qt-5-15-11-core22-sdk/current${CRAFT_CMAKE_ARGS:+:$CRAFT_CMAKE_ARGS}
    skrooge:
        after:
        - kde-neon
        parse-info:
        - usr/share/metainfo/org.kde.skrooge.appdata.xml
        plugin: cmake
        build-packages:
        - libgrantlee5-dev
        - libkf5kdelibs4support-dev
        - libofx-dev
        - libqca-qt5-2-dev
        - libsqlcipher-dev
        - xsltproc
        - qtbase5-private-dev
        - qtwebengine5-dev
        stage-packages:
        - libgrantlee-templates5
        - libgrantlee-textdocument5
        - libkf5kdelibs4support5
        - libofx7
        - libqca-qt5-2
        - libsqlcipher0
        - freeglut3
        - libslang2
        - libkf5grantleetheme-plugins
        - qtwebengine5-dev
        source: .
        source-type: local
        cmake-parameters:
        - -DKDE_INSTALL_USE_QT_SYS_PATHS=FALSE
        - "-DCMAKE_INSTALL_PREFIX=/usr"
        - "-DCMAKE_BUILD_TYPE=Release"
        - "-DENABLE_TESTING=OFF"
        - "-DBUILD_TESTING=OFF"
        - "-DKDE_SKIP_TEST_SETTINGS=ON"
        - "-DCMAKE_FIND_ROOT_PATH=/usr\\;$CRAFT_STAGE\\;/snap/kf5-5-110-qt-5-15-11-core22-sdk/current"
        - "-DKDE_INSTALL_PLUGINDIR=/usr/lib/$CRAFT_ARCH_TRIPLET/qt5/plugins/"
        prime:
        - "-usr/lib/*/cmake/*"
        - "-usr/include/*"
        - "-usr/share/ECM/*"
        - "-usr/share/doc/*"
        - "-usr/share/man/*"
        - "-usr/share/icons/breeze-dark*"
        - "-usr/bin/X11"
        - "-usr/lib/gcc/$CRAFT_ARCH_TRIPLET/6.0.0"
        - "-usr/lib/aspell/*"
        - "-usr/share/lintian"
        build-environment: &id005
        - *id001
        - *id002
        - *id003
        - *id004
    cleanup:
        after:
        - kde-neon
        - skrooge
        plugin: nil
        override-prime:  |
            set -eux
            # # Unused libraries found by linter
