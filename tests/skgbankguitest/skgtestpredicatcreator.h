/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGTESTPREDICATCREATOR_H
#define SKGTESTPREDICATCREATOR_H
/** @file
 * This file is a test for SKGPredicatCreator component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <qtest.h>

/**
 * A unit test
 */
class SKGTESTPredicatCreator: public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void Test();
};
#endif
